include common-files/base.mk
SHELL := bash

# keep-sorted start
java21-badge: docker_badge_java21
java21-push: docker_push_java21
java21-test-rmi: docker_rmi_test_java21
java21-test: docker_build_test_java21
java21-topic: docker_topic_java21
java21: docker_build_java21
# keep-sorted end
