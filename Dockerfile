FROM openjdk:21-slim

RUN apt-get update \
    && apt-get -y --no-install-recommends install docker.io \
        git \
        make \
        moreutils \
        python3 \
        python3-pip \
        ssh \
        tar \
        curl \
        maven \
        ca-certificates \
    && install -m 0755 -d /etc/apt/keyrings \
    && apt-get -y purge --auto-remove \
    && pip3 install --no-cache-dir --break-system-packages anybadge elementpath \
    && GJF=1.19.1 \
    && TOKEI=12.1.2 \
    && curl -fsSL https://download.docker.com/linux/debian/gpg -o /etc/apt/keyrings/docker.asc \
    && curl --location --fail "https://github.com/google/google-java-format/releases/download/v${GJF}/google-java-format-${GJF}-all-deps.jar" -o /opt/google-java-format.jar \
    && curl --location --fail "https://github.com/XAMPPRocky/tokei/releases/download/v${TOKEI}/tokei-x86_64-unknown-linux-gnu.tar.gz" -o /usr/local/bin/tokei.tar.gz \
    && tar -vxzf /usr/local/bin/tokei.tar.gz \
    && chmod a+r /etc/apt/keyrings/docker.asc \
    && apt-get -y clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* /var/tmp* \
    && rm -rf /usr/local/bin/tokei.tar.gz \
    && echo \
         "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
         $(. /etc/os-release && echo ""$VERSION_CODENAME"") stable" | \
         tee /etc/apt/sources.list.d/docker.list > /dev/null \
    && apt-get update \
    && apt-get -y clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* /var/tmp* \
    && rm -rf /usr/local/bin/tokei.tar.gz \
    && echo \
         "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/debian \
         $(. /etc/os-release && echo ""$VERSION_CODENAME"") stable" | \
         tee /etc/apt/sources.list.d/docker.list > /dev/null \
    && apt-get update \
    && apt-get -y clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* /var/tmp*

COPY common-files/java/* /usr/local/bin

RUN chmod +x /usr/local/bin/*